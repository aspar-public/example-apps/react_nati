import { createContext, useEffect, useState } from "react";
import { getUserByToken } from "../services/UserServices";

const AuthContext = createContext({
    // ... other context values
    currentUser: null,
    setCurrentUser: () => {},
  });

export default AuthContext;

export const AuthProvider =({children}) => {
    let [role, setRole] = useState(localStorage.getItem('userRole') || null);
    let [jwtToken, setJwtToken] = useState(localStorage.getItem('jwtToken') || null);

    const [currentUser, setCurrentUser] = useState(null);

    useEffect(() => {
      const fetchUser = async () => {
        if (jwtToken) {
          try {
            const userData = await getUserByToken(jwtToken);
            setCurrentUser(userData);
          } catch (err) {
            console.error('Failed to fetch user:', err);
          }
        }
      };
  
      fetchUser();
    }, [jwtToken, setCurrentUser]);

    let contextData = {
        role,
        jwtToken,
        setJwtToken, 
        setRole,
    }
    return (
        <AuthContext.Provider value={{...contextData, currentUser, setCurrentUser}}>
            {children}
        </AuthContext.Provider>
    )

}