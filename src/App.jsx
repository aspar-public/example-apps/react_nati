import { Suspense, useContext, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'

import { AuthProvider } from './util/AuthContext';


import './App.css';
import './Responsive.css';
import './components/pages/register/Register.css';
import Header from './components/header/Header';
import Home from './components/pages/home/Home';
import Settings from './components/pages/account/Settings';
import Register from './components/pages/register/Register'
import Login from './components/pages/login/Login';
import Contacts from './components/pages/entities/contacts/Contacts';
import Products from './components/pages/entities/products/Products';
import Password from './components/pages/account/Password';
import Footer from './components/footer/Footer';

import Activation from './components/pages/account/Activation';
import ResetPasswordInit from './components/pages/account/ResetPassInit';
import ResetPasswordFinish from './components/pages/account/ResetPassFinish';


function App() {


  return (
    <AuthProvider>
    <Suspense fallback="...loading">
    <Router>
      <Header />

      <Routes>
      <Route path='/' element={<Home/>}> </Route>
      <Route path='/account/register' element={<Register />}></Route>
      <Route path='/account/login' element={<Login />}></Route>
      <Route path='/entities/contacts' element={<Contacts />}> </Route>
      <Route path='/entities/products' element={<Products />}> </Route>
      <Route path='/account/settings' element={<Settings />}> </Route>
      <Route path='/account/password' element={<Password />}> </Route>
      <Route path='/account/reset/request' element={<ResetPasswordInit />}> </Route>
      <Route path='/account/reset/finish' element={<ResetPasswordFinish />}> </Route>
      <Route path='/account/activate' element={<Activation />}> </Route>
     
      </Routes>

      <Footer />

    </Router>
    </Suspense>
    </AuthProvider>
  );
};

export default App;

