import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import Backend from 'i18next-http-backend';



i18n.use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        backend: {
           
            loadPath:"/assets/i18n/{{ns}}/{{lng}}.json"
        },
        debug: false,
        fallbackLng: 'en',
        //it can have multiple ns in case needed
        ns: ["common", "home", "profile"],
        interpolation: {
            escapeValue: false,
            formatSeparator: ","
        },
        react: {
            with: true
        }
    });

export default i18n;