import Dropdown from './Dropdown';
import { useState,useEffect, useRef } from "react";
import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link, useNavigate } from "react-router-dom";

const MenuItems = ({ items,closeNav }) => {
    const navigate = useNavigate();
    const [dropdown, setDropdown] = useState(false);
    const ref = useRef();

    const closeDropdown = () => {
      dropdown && setDropdown(false);
    };

    
    useEffect(() => {
      const handler = (event) => {
       if (dropdown && ref.current && !ref.current.contains(event.target)) {
        setDropdown(false);
       }
      };
      document.addEventListener("mousedown", handler);
      document.addEventListener("touchstart", handler);
      return () => {
       // Cleanup the event listener
       document.removeEventListener("mousedown", handler);
       document.removeEventListener("touchstart", handler);
      };
     }, [dropdown]);

     const onMouseEnter = () => {
     window.innerWidth > 960 && 
      setDropdown(true);
     };
     
     const onMouseLeave = () => {
      window.innerWidth > 960 && 
      setDropdown(false);
     };
     
  return (
    <li className="menu-items" 
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
    onClick={closeDropdown}>
      {items.submenu ? (
        <>
          <button className='menu-items-button' type="button" aria-haspopup="menu"   
          aria-expanded={dropdown ? "true" : "false"}
      onClick={() => setDropdown((prev) => !prev)}>
            <FontAwesomeIcon className='topnav-icon' icon={items.icon}/>
            {items.title}{' '} 
            <FontAwesomeIcon className='menu-icon' icon={faCaretDown} />
          </button>
          <Dropdown submenus={items.submenu}   dropdown={dropdown} closeNav={closeNav}/>
        </>
      ) : (
        <Link className='menu-items-link' onClick={()=>closeNav()} to={items.url}><FontAwesomeIcon className='topnav-icon' icon={items.icon}/>{items.title} </Link>
      )}
        
    </li>
  );
};
export default MenuItems;