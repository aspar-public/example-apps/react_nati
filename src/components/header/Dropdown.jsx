import { Link, useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import AuthContext from "../../util/AuthContext";
import { useContext } from "react";

const Dropdown = ({ submenus, dropdown, closeNav }) => {
  const navigate = useNavigate();
  const { setJwtToken, setRole, setCurrentUser } = useContext(AuthContext);

  const onLogout = () => {
    localStorage.removeItem('jwtToken');
    localStorage.removeItem('userRole');
    setRole(null);
    setJwtToken(null);
    setCurrentUser(null);
    closeNav();
    
    navigate('/');
  }

  const onMenuClick = (url) => {
    
    if (url) {
      if (url && window.location.pathname !== url) {
        navigate(url);
      }
    }
    // Close the navbar when an item is clicked in mobile view
    closeNav();  // Assuming closeNav sets the nav expanded state to false
};

    return (
      <ul className={`dropdown ${dropdown ? "show" : ""}`}>
        {submenus.map((submenu, index) => (
          <li key={index} className="menu-items" >
            {(submenu.title=='Sign out' || submenu.title== "Изход"  )
            ?   <button className='menu-items-button'  onClick={onLogout}>
            <FontAwesomeIcon className='topnav-icon' icon={submenu.icon}/> {submenu.title}
            </button>
            :   <Link to={submenu.url} onClick={onMenuClick}>
            <FontAwesomeIcon className='topnav-icon' icon={submenu.icon}/> {submenu.title}
          </Link>}
          
          </li>
        ))}
      </ul>
    );
  };
  
  export default Dropdown;