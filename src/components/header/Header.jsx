import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import i18next from "i18next";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Navbar from './Navbar';
import Language from "./Language";
import Logo from '../../images/logo.png';
import styles from './Header.module.css'



const Header = () => {
    const [t, i18n] = useTranslation('common');

    useEffect(()=> {
        if(localStorage.getItem('i18nextLng')?.length>2) {
            i18next.changeLanguage('en')
        }
    }, []);
    const handlerLangChange = (e) => {
        i18n.changeLanguage(e.target.value);
        console.log(e.target.value);
    }

    const [isNavExpanded, setIsNavExpanded] = useState(false);
    const closeNav = () => {
        setIsNavExpanded(false);
     
      };

   
    return ( 
        <section className="header-container">
            <button className="hamburger"
             onClick={()=> {
							setIsNavExpanded(!isNavExpanded);
						}}><FontAwesomeIcon className='' icon={faBars} /></button>
            <div className="logo">
                <img className={styles.logoImg} src={Logo} alt="logo"></img>
                <p className="logo-info">ExampleApp01</p>
            </div>
            <nav  className={
          isNavExpanded ? "topNav expanded" : "topNav"
        } >
               

                <Navbar closeNav={closeNav}/>
 
                <Language closeNav={closeNav}/>
               
                </nav>
                
        </section>

     );
}
 
export default Header;