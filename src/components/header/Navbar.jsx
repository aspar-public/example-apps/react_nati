
import { useTranslation } from "react-i18next";
import i18next from "i18next";
import { useNavigate} from 'react-router-dom';
import MenuItems from './MenuItems';
import { faHome, faTableList, faUsersGear, faUser, faLock, faWrench, faRightFromBracket, 
        faRightToBracket, faUserPlus, faUsers, faEye, faTachometer, faGears, faHeart, 
        faTasks, faBook, faStarOfLife, faAddressBook } from "@fortawesome/free-solid-svg-icons";

import { useContext, useEffect, useState } from "react";
import AuthContext from "../../util/AuthContext";



const Navbar = ( {closeNav} ) => {
    const [t, i18n] = useTranslation('common');
    const navigate = useNavigate();
    
    const menuItemsGuest = [
      {
          title:  t('home'),
          url: '/',
          icon: faHome
      },
  
        {
          title: t('account'),
          icon: faUser,
          //url: '',
          submenu: [
           
              {
                title: t('signin'),
                url: 'account/login',
                icon: faRightToBracket,
              },
              {
                title: t('register'),
                url: 'account/register',
                icon: faUserPlus
              },
          ]
        },
  
      // ...
    ];
   
    const menuItemsUser = [
      {
          title:  t('home'),
          url: '/',
          icon: faHome
      },
  
      {
        title: t('entities'),
        icon: faTableList,
        submenu: [
          {
            title: t('products'),
            url: 'entities/products',
            icon: faStarOfLife
          },
          {
            title: t('contact'),
            url: 'entities/contacts',
            icon: faAddressBook
          },
          
        ],
      },
        {
          title: t('account'),
          icon: faUser,
          //url: '',
          submenu: [
              {
                title: t('settings'),
                url: 'account/settings',
                icon: faWrench,
              },
              {
                title: t('password'),
                url: 'account/password',
                icon: faLock
              },
              {
                title: t('signout'),
                icon: faRightFromBracket,
                //url: '',
                
              },
          
          ]
        },
      ];
  
    const menuItemsAdmin = [
        {
            title:  t('home'),
            url: '/',
            icon: faHome
        },
    
        {
          title: t('entities'),
          icon: faTableList,
          submenu: [
            {
              title: t('products'),
              url: 'entities/products',
              icon: faStarOfLife
            },
            {
              title: t('contact'),
              url: 'entities/contacts',
              icon: faAddressBook
            },
            
          ],
        },
        {
            title: t('administration'),
            icon: faUsersGear,
            //url: 'administration',
            submenu: [
              {
                title: t('userManagement'),
                url: 'admin/user-management',
                icon: faUsers
              },
              {
                title: t('userTracker'),
                url: 'admin/user-tracker',
                icon: faEye
              },
              {
                title: t('metrics'),
                url: 'admin/metrics',
                icon: faTachometer
              },
              {
                title: t('health'),
                url: 'admin/health',
                icon: faHeart
              },
              {
                title: t('configuration'),
                url: 'admin/confugiration',
                icon: faGears
              },
              {
                title: t('logs'),
                url: 'admin/logs',
                icon: faTasks
              },
              {
                title: t('api'),
                url: 'admin/api',
                icon: faBook
              },
              
            ],
          },
          {
            title: t('account'),
            icon: faUser,
            //url: '',
            submenu: [
                {
                  title: t('settings'),
                  url: 'account/settings',
                  icon: faWrench,
                },
                {
                  title: t('password'),
                  url: 'account/password',
                  icon: faLock
                },
                {
                  title: t('signout'),
                  icon: faRightFromBracket,
                  //url: '',
                  
                },
             
            ]
          },
    
        // ...
      ];

      
      const authContext = useContext(AuthContext);
      const { role, jwtToken } = authContext;
   
       // Function to generate menu items based on the current language
  const getMenuItems = () => {
    if (role) {
      if (role.includes("ROLE_USER")) { 
        if (role.includes("ROLE_ADMIN")) {
          return menuItemsAdmin.map(item => ({ ...item, title: t(item.title) }));
      } else {
        return menuItemsUser.map(item => ({ ...item, title: t(item.title) }));
        }
      }
    } 
    return menuItemsGuest.map(item => ({ ...item, title: t(item.title) }));
  };
  const [menuItems, setMenuItems] = useState(getMenuItems());


         // Update menu items when role, jwtToken, or language changes
        useEffect(() => {
         
          setMenuItems(getMenuItems());
    
        }, [role, jwtToken, i18n.language]); 
      
  

    return (
      <nav>
            <ul className="menus">
        {menuItems.map((menu, index) => {
          return <MenuItems items={menu} key={index} closeNav={closeNav}/>;
        })}
      </ul>
      </nav>
    );
  };
  
  export default Navbar;