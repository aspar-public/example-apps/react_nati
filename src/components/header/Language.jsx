
import { useState, useEffect } from "react";
import { faCaretDown, faFlag } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from "react-router-dom";
import i18next from "i18next";
import { useTranslation } from "react-i18next";
import Cookies from "js-cookie";

const Language = ({closeNav}) => {
    const languages = [
        { name: "English", code: "en" },
        { name: "Български", code: "bg" },
      ];
    
      const {t, i18n} = useTranslation();

      const storedLocale = localStorage.getItem("i18next") || "en";
      const currentLocale = Cookies.get("i18next") || storedLocale;

      const [language, setLanguage] = useState(currentLocale);
      

      useEffect(() => {
        // Set the language in i18next and update localStorage when the language changes
        i18next.changeLanguage(language);
        localStorage.setItem("i18next", language);
        Cookies.set("i18next", language);
      }, [language, i18n]);
    
  
  const handleChangeLocale = (e) => {
    const lang = e.target.value;
    setLanguage(lang);
    i18next.changeLanguage(lang);
    setShowLng(false);
    closeNav();
  };

  const [showLng, setShowLng] = useState(false);
    const handleMenuMouseEnter = () => {
    setShowLng(true);
  };
    const handleMenuMouseLeave = () => {
    setShowLng(false);
  };
    const handleLanguageBtnMouseEnter = () => {
    setShowLng(true);
  };
    const handleLanguageBtnMouseLeave = () => {
    setShowLng(false);
};




  return (

    <div className="navItem" onMouseEnter={handleLanguageBtnMouseEnter} onMouseLeave={handleLanguageBtnMouseLeave}>
   
    <button className="lngBtn" 
        onClick={()=> setShowLng(!showLng)}
    
         >
             <FontAwesomeIcon className='topnav-icon' icon={faFlag}/>
          {t('language')} 
          <FontAwesomeIcon className='menu-icon' icon={faCaretDown} />
    </button>
   
    {showLng ? <ul className="switcher" 
                  onMouseEnter={handleMenuMouseEnter} 
                  onMouseLeave={handleMenuMouseLeave}
                  > 
      {languages.map(({ name, code, index }) => (
        <li className="lngSwitcherItem" key={index} >
           <button className="lngSwitcherBtn" onClick={() => handleChangeLocale({ target: { value: code } })}>
         {name}</button> 
        </li>
      ))}
      </ul> : ""}


  
  </div>
  );
};
export default Language;