import { useState } from 'react';
import { useSearchParams, Link } from 'react-router-dom';
import i18next from "i18next";
import { useTranslation } from "react-i18next";
import { activateAccount } from '../../../services/AccountServices';

function Activation() {
    const [t, i18n] = useTranslation('profile');
  // React Router hook to get access to the search parameters
  let [searchParams, setSearchParams] = useSearchParams();
    // Extract the 'key' parameter from the URL
  const key = searchParams.get('key');

  const [showResponse, setShowResponse] = useState(false);
  const [showActivated, setShowActivated] = useState(false);
  
  

  // A function to handle the activation process
  const handleActivation = async () => {
    // Here you would typically make an API call to your backend service
    try {
        await activateAccount(key);
        setShowActivated(true);
    } catch (error) {
        console.log(error);
    }
    setShowResponse(true);
  }
  handleActivation();

  // Render the component
  return (
    <section className='activationPage'>
          <div className="form-container">
      <h2 className='form-title'>{t("activationTitle")}</h2>
      {showResponse && (<>
                {showActivated ? 
                  <div className='greenMsg'>
                      <p>{t("activationMsg1")} <Link to="/account/login" className="link">{t("activationMsg2")}</Link></p>
                  </div> 
              :  <div className='redMsg'>
                      <p>{t("activationMsg3")} </p>
                  </div> 
              }
            </>
      )}


      
        </div>
    </section>
  );
}

export default Activation;