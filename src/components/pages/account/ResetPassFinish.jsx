import { useTranslation } from "react-i18next";
import i18next from "i18next";
import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { useSearchParams, Link } from 'react-router-dom';
import { resetPasswordFinish } from "../../../services/AccountServices";


 const ResetPasswordFinish = () => {
    const [t, i18n] = useTranslation('profile');
    let [searchParams, setSearchParams] = useSearchParams();

    const key = searchParams.get('key');

	const { register, handleSubmit, name, getValues, setValue, formState, reset, isValid, setError,
		formState:{ errors, touchedFields, dirtyFields, isSubmitSuccessful} } = useForm({
				defaultValues: {
					password: "",
                    confirmPassword: ""
				},
				mode: "onTouched"
	});

	const onError = (errors) => console.log("Error", errors, "Touched:", touchedFields, "Dirty:", dirtyFields);
		const handleError = (errors) => {};

        const registerOptions = {
            password: {
              required: t("errorPasswordRequired"),
            },
            confirmPassword: {
              required: t("errorPasswordConfirmRequired"),
              validate:  value  => value === getValues('password') || t("errorPasswordMatch")
            }
          }

		const [showSuccess, setShowSuccess] = useState(false);

		useEffect(() => {
		  if (isSubmitSuccessful) {
			setShowSuccess(true);
		  }
		}, [isSubmitSuccessful]);
	
	
	const onSubmit = async(data) => {
        console.log('Activating key:', key);
        const reqData = {
            key: key,
            newPassword: data.password
        }
        console.log(reqData);
		try {
			await resetPasswordFinish(reqData);
			setShowSuccess(true);
		} catch (error) {
			console.log(error);
		}
	}


    return (
        <section className="registerPage">
            <div className="form-container">
				
					<h2 className="form-title">{t('passResetTitle')}</h2>
					{showSuccess ? (
							   <div className='greenMsg'>
                               <p>{t("newPasswordMsg1")} <Link to="/account/login" className="link">{t("activationMsg2")}</Link></p>
                           </div>	)
							: (	<><div className="yellowMsg">
							<p className="success">{t("passResetMsg3")}</p>
								</div>
									<form  className="form" onSubmit={handleSubmit(onSubmit)}>
								
							<div className="form-field">
								<label className='form-label' htmlFor="password">{t('newPassword')}</label>
								<input type="password" 
									 className={`form-input ${errors.password ? 'input-danger' : ''}`}
									name="password"
									id="password"
                                    placeholder={t('newPassword')}
									{...register( "password", registerOptions.password )}

									/>
									{touchedFields.password && (	
									<small className="text-danger">{errors?.password?.message}</small>
									)}

								<div className="password-strength">
									<p>{t(  "passwordStrengthLbl")}</p>
									<ul className="pass-strength-list">
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
									</ul>
									
								</div>
								
							</div>

                			<div className="form-field">
								<label className='form-label' htmlFor="confirmPassword">{t('newPasswordConfirm')}</label>
								<input type="password"
					 				 className={`form-input ${errors.confirmPassword ? 'input-danger' : ''}`}
									name="confirmPassword"
									id="confirmPassword"
                                    placeholder={t('newPasswordConfirm')}
									{...register( "confirmPassword", registerOptions.confirmPassword)}

								/>
									{touchedFields.confirmPassword && (
								<small className="text-danger">{errors?.confirmPassword?.message}</small>
									)}
								</div>

									<button type="submit" className="button form-btn">{t('newPasswordBtn')}</button>				
								</form>
								</>	)
						}	
        
			</div>
		</section>
    );
}
export default ResetPasswordFinish;