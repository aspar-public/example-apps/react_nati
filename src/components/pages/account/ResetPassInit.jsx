import { useTranslation } from "react-i18next";
import i18next from "i18next";
import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { resetPasswordInit } from "../../../services/AccountServices";


 const ResetPasswordInit = () => {
    const [t, i18n] = useTranslation('profile');

	const { register, handleSubmit, name, getValues, setValue, formState, reset, isValid, setError,
		formState:{ errors, touchedFields, dirtyFields, isSubmitSuccessful} } = useForm({
				defaultValues: {
					email: "",
				},
				mode: "onTouched"
	});

	const onError = (errors) => console.log("Error", errors, "Touched:", touchedFields, "Dirty:", dirtyFields);
		const handleError = (errors) => {};

		const registerOptions = {
		  email: { required: t("errorEmailRequired") },
		
		}

		const [showSuccess, setShowSuccess] = useState(false);

		useEffect(() => {
		  if (isSubmitSuccessful) {
			setShowSuccess(true);
		  }
		}, [isSubmitSuccessful]);
	
	
	const onSubmit = async(data) => {
		try {
			await resetPasswordInit(data.email);
			setShowSuccess(true);
		} catch (error) {
			console.log(error);
		}
	}


    return (
        <section className="registerPage">
            <div className="form-container">
				
					<h2 className="form-title">{t('passResetTitle')}</h2>
					{showSuccess ? (
							<div className="greenMsg">
								<p className="success">{t("passResetMsg2")}</p>
							</div>	)
							: (	<><div className="yellowMsg">
							<p className="success">{t("passResetMsg1")}</p>
								</div>
									<form  className="form" onSubmit={handleSubmit(onSubmit)}>
									<div className="form-field">
										<label className='form-label' htmlFor="email">{t('emailLbl')}</label>
										<input type="text"
											  className={`form-input ${errors.email ? 'input-danger' : ''}`}
											name="email"
											id="email"
											placeholder={t('emailPH')}
											{...register("email", registerOptions.email)}
											
										/>
										{touchedFields.login && (
										<small className="text-danger">{errors?.email?.message}</small>
									)}
									</div>
									<button type="submit" className="button form-btn">{t('passResetBtn')}</button>				
								</form>
								</>	)
						}	
        
			</div>
		</section>
    );
}
export default ResetPasswordInit;