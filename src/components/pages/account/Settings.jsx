import { useTranslation } from "react-i18next";
import i18next from "i18next";
import { useContext, useEffect, useState } from "react";

import AuthContext from "../../../util/AuthContext";
import { useForm } from "react-hook-form";
import { editUser, getUserByToken } from "../../../services/UserServices";


const Settings = () => {
   
    const [t, i18n] = useTranslation('profile');

    const authContext = useContext(AuthContext);
    const { jwtToken, currentUser, setCurrentUser } = authContext;

    const { register, handleSubmit, name, getValues, setValue, formState, reset, isValid, setError,
		formState:{ errors, touchedFields, dirtyFields, isSubmitSuccessful} } = useForm({
				defaultValues: {
					firstName: "",
					lastName: "",
                    email: "",
                    imageUrl: "",
                    langKey: ""
				},
				mode: "onTouched"
	});

    const registerOptions = {
        email: { required: t("errorEmailRequired") },
      }
    

    useEffect(() => {
        if(currentUser) {
            reset({
                firstName: currentUser.firstName ?? '',
                lastName: currentUser.lastName?? '',
                email: currentUser.email?? '',
                imageUrl: currentUser.imageUrl?? '',
                langKey: currentUser.langKey?? '',
              });
        }

        
    }, [currentUser, reset]);

    
    const [showSuccess, setShowSuccess] = useState(false);

    useEffect(() => {
      if (isSubmitSuccessful) {
        setShowSuccess(true);
      }
    }, [isSubmitSuccessful]);


    const onSubmit = async (data) => {
		
        let user = {
            login: currentUser.login,
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            imageUrl: data.imageUrl,
            langKey: data.langKey,
        }
        try {
          
            await editUser(user, jwtToken);
            const updatedUser = await getUserByToken(jwtToken);
            setCurrentUser(updatedUser);
            
            //handle success
            setShowSuccess(true);
         
        } catch (error) {
            //handle error
            setError("settings", {
                type: "manual",
                message: "Unable to update user",
              });
            console.error('Error')
        }
    }
  

    return ( 
        <section className="settingsPage">
        <div className="form-container">
            
                <h2 className="form-title">{t('settingsTitle')} [{currentUser?.login ?? 'Loading...'}]</h2>
                {showSuccess && (
							<div className="greenMsg">
								<p className="success">{t("settingsSaved")}</p>
							</div>	)}	
                    <form  className="password" onSubmit={handleSubmit(onSubmit)} >
                        <div className="form-field">
                            <label className='form-label' htmlFor="firstName">{t('firstName')}</label>
                            <input type="text"
                                className="form-input" 
                                name='firstName' 
                                id='firstName'
                                {...register("firstName")}
                                />
                        </div>
                    

                        <div className="form-field">
                            <label className='form-label' htmlFor="lastName">{t('lastName')}</label>
                            <input type="text" 
                                className="form-input"
                                 name='lastName' 
                                id='lastName'
                    
                                {...register("lastName")}
                                 />
                          
                        </div>

                        <div className="form-field">
								<label className='form-label' htmlFor="email">{t('emailLbl')}</label>
								<input type="text"
					 				className="form-input" 
					 				name='email'
									id='email' 
                                    {...register("email", registerOptions.email)}
								/>
						</div>

                        <div className="form-field">
								<label className='form-label' htmlFor="imageUrl">{t('imageUrlLbl')}</label>
								<input type="text"
									  className="form-input"
									name="imageUrl"
									id="imageUrl"
                                    placeholder={t('imageUrlPH')}
									{...register("imageUrl")}
									
								/>
						
							</div>

                        <div className="form-field">
                            <label className='form-label' htmlFor="langKey">{t('language')}</label>
                           
                        <select  className="form-input" 
                                name="langKey" 
                                id="langKey"
                                {...register("langKey", {value: currentUser?.langKey})}
                                >
                                  <option value="en">English</option>
                                  <option value="bg">Български</option>
                        </select>
                          
                        </div>

           
        
                        <button type="submit" className="button form-btn">{t('save')}</button>				
                    </form>
    
    </div>
    

    </section>
    )
}

export default Settings;