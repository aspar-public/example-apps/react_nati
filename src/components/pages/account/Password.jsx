import { useTranslation } from "react-i18next";
import i18next from "i18next";
import { useContext, useEffect, useState } from "react";

import AuthContext from "../../../util/AuthContext";
import { useForm } from "react-hook-form";
import { editPassword } from "../../../services/AccountServices";


const Password = () => {

    const [t, i18n] = useTranslation('profile');
    const authContext = useContext(AuthContext);
    const { jwtToken, currentUser, setCurrentUser } = authContext;

    const { register, handleSubmit, name, getValues, setValue, formState, reset, isValid, setError,
		formState:{ errors, touchedFields, dirtyFields, isSubmitSuccessful} } = useForm({
				defaultValues: {
					currentPassword: "",
					newPassword: "",
                    confirmPassword: ""
				},
				mode: "onTouched"
	});

    const registerOptions = {
        currentPassword: {
            required: t("errorPasswordRequired"),
        },
        newPassword: {
          required: t("errorPasswordRequired"),
        },
        confirmPassword: {
          required: t("errorPasswordConfirmRequired"),
          validate:  value  => value === getValues('newPassword') || t("errorPasswordMatch")
        }
      }

      const [showSuccess, setShowSuccess] = useState(false);
      useEffect(() => {
        if (isSubmitSuccessful) {
          setShowSuccess(true);
        }
      }, [isSubmitSuccessful]);

      
   

      const onSubmit = async (data) => {
        console.log(data);
        const formData = {
            currentPassword: data.currentPassword,
            newPassword: data.newPassword
        }
        console.log(formData);
        try {
            await editPassword(formData, jwtToken);
            setShowSuccess(true);
            
        } catch (error) {
                 //handle error
                 setError("password", {
                    type: "manual",
                    message: t("passwordChangeErr"),
                  });
                console.error('Error');
             
        }

      }

    return ( 
        <section className="passwordPage">
        <div className="form-container">
            
                <h2 className="form-title">{t('passwordTitle')} [{currentUser?.login ?? 'Loading...'}]</h2>
                    <form  className="password" onSubmit={handleSubmit(onSubmit)}>
                    {showSuccess && (
							<div className="greenMsg">
								<p className="success">{t("passwordChanged")}</p>
							</div>	)}	
                            {errors.password && (
							<div className="redMsg">
								<p className="success">{t("passwordChangeErr")}</p>
							</div>	)}	
                        <div className="form-field">
                            <label className='form-label' htmlFor="currentPassword">{t('currentPasswordLbl')}</label>
                            <input type="password"
                                className={`form-input ${errors.currentPassword ? 'input-danger' : ''}`}
                                name='currentPassword' 
                                id='currentPassword'
                                placeholder={t('currentPasswordLbl')}
                                {...register( "currentPassword", registerOptions.currentPassword )}
                                
                                />
                                {touchedFields.currentPassword && (	
									<small className="text-danger">{errors?.currentPassword?.message}</small>
									)}
                        </div>
                    

                        <div className="form-field">
								<label className='form-label' htmlFor="newPassword">{t('passwordLbl')}</label>
								<input type="password" 
									 className={`form-input ${errors.newPassword ? 'input-danger' : ''}`}
									name="newPassword"
									id="newPassword"
                                    placeholder={t('passwordPH')}
									{...register( "newPassword", registerOptions.newPassword )}

									/>
									{touchedFields.newPassword && (	
									<small className="text-danger">{errors?.newPassword?.message}</small>
									)}
                                
                                <div className="password-strength">
									<p>{t('passwordStrengthLbl')}</p>
									<ul className="pass-strength-list">
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
									</ul>
									
								</div>
                        </div>

                        <div className="form-field">
								<label className='form-label' htmlFor="confirmPassword">{t('rePassLbl')}</label>
								<input type="password"
					 				 className={`form-input ${errors.confirmPassword ? 'input-danger' : ''}`}
									name="confirmPassword"
									id="confirmPassword"
                                    placeholder={t('rePassPH')}
									{...register( "confirmPassword", registerOptions.confirmPassword)}

								/>
									{touchedFields.confirmPassword && (
								<small className="text-danger">{errors?.confirmPassword?.message}</small>
									)}
								</div>
           
        
                        <button type="submit" className="button form-btn">{t('save')}</button>				
                    </form>
    
    </div>
    

    </section>
     );
}

export default Password;
