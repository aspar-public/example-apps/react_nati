import { useTranslation } from "react-i18next";
import i18next from "i18next";
import { Link } from "react-router-dom";
import { useState, useEffect } from "react";
import { registerUser } from "../../../services/UserServices";
import { useForm } from "react-hook-form";

 const Register = () => {
    const [t, i18n] = useTranslation('profile');
	const storedLanguage = localStorage.getItem('lang');
	const defaultLang = storedLanguage || 'en';
	

	const { register, handleSubmit, name, getValues, setValue, formState, reset, isValid,
		formState:{ errors, touchedFields, dirtyFields, isSubmitSuccessful} } = useForm({
				defaultValues: {
					login: "",
					firstName: "",
					lastName: "",
					email: "",
					imageUrl: "",
					password: "",
					confirmPassword: "",
					langKey: defaultLang
				},
				mode: "onTouched"
	});

	useEffect(() => {
		setValue('langKey', defaultLang);
		reset({}, { errors: true });
	  }, [defaultLang, setValue]);

	  
	  const [showSuccess, setShowSuccess] = useState(false);

	  useEffect(() => {
		if (isSubmitSuccessful) {
		  setShowSuccess(true);
		}
	  }, [isSubmitSuccessful]);

	
	const onSubmit = async (data) => {
		
			try {
				if(data.langKey==='') {
					data.langKey=defaultLang
				}
				
				//console.log("Form:", data)
				await registerUser(data);
				
				//handle success
				setShowSuccess(true);

				reset({
					login: "",
					firstName: "",
					lastName: "",
					email: "",
					imageUrl: "",
					password: "",
					confirmPassword: "",
					langKey: defaultLang,
				  });
			
			} catch (error) {
				console.log(data);
				//handle error
				setError("login", {
					type: "manual",
					message: "Cannot register",
				  });
				console.error('Error')
			}
		}
	
		const onError = (errors) => console.log("Error", errors, "Touched:", touchedFields, "Dirty:", dirtyFields);
		const handleError = (errors) => {};

		const registerOptions = {
		  login: { required: t("errorUsernameRequired") },
		  email: { 
		  required: t("errorEmailRequired"),
		  },
		  password: {
			required: t("errorPasswordRequired"),
			minLength: {
			  value: 4,
			  message: t("errorPasswordMinLength")
			}
		  },
		  confirmPassword: {
			required: t("errorPasswordConfirmRequired"),
			validate:  value  => value === getValues('password') || t("errorPasswordMatch")
		  }
		}
		

    return (
        <section className="registerPage">
            <div className="form-container">
				
					<h2 className="form-title">{t('registration')}</h2>
					{!showSuccess && (
						<form onSubmit={handleSubmit(onSubmit, onError)} className="register">
            				<div className="form-field">
								<label className='form-label' htmlFor="login">{t('usernameLbl')}</label>
								<input type="text"
									  className={`form-input ${errors.login ? 'input-danger' : ''}`}
									name="login"
									id="login"
                                    placeholder={t('usernamePH')}
									{...register("login", registerOptions.login)}
									
								/>
							{touchedFields.login && (
								<small className="text-danger">{errors?.login?.message}</small>
							)}
							</div>

							<div className="form-field">
								<label className='form-label' htmlFor="firstName">{t('firstNameLbl')}</label>
								<input type="text"
									  className="form-input"
									name="firstName"
									id="firstName"
                                    placeholder={t('firstNamePH')}
									{...register("firstName")}
									
								/>
							</div>

							<div className="form-field">
								<label className='form-label' htmlFor="lastName">{t('lastNameLbl')}</label>
								<input type="text"
									  className="form-input"
									name="lastName"
									id="lastName"
                                    placeholder={t('lastNamePH')}
									{...register("lastName")}
									
								/>
						
							</div>
							
                			<div className="form-field">
								<label className='form-label' htmlFor="email">{t('emailLbl')}</label>
								<input type="email" 
									  className={`form-input ${errors.email ? 'input-danger' : ''}`}
									 name="email"
									 id="email"
                                    placeholder={t('emailPH')}
									{...register("email", registerOptions.email)}

					 				/>
								{touchedFields.email && (
								<small className="text-danger">{errors?.email?.message}</small>
								)}
							</div>

							<div className="form-field">
								<label className='form-label' htmlFor="imageUrl">{t('imageUrlLbl')}</label>
								<input type="text"
									  className="form-input"
									name="imageUrl"
									id="imageUrl"
                                    placeholder={t('imageUrlPH')}
									{...register("imageUrl")}
									
								/>
						
							</div>
               
				
							<div className="form-field">
								<label className='form-label' htmlFor="password">{t('passwordLbl')}</label>
								<input type="password" 
									 className={`form-input ${errors.password ? 'input-danger' : ''}`}
									name="password"
									id="password"
                                    placeholder={t('passwordPH')}
									{...register( "password", registerOptions.password )}

									/>
									{touchedFields.password && (	
									<small className="text-danger">{errors?.password?.message}</small>
									)}

								<div className="password-strength">
									<p>{t(  "passwordStrengthLbl")}</p>
									<ul className="pass-strength-list">
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
										<li className="pass-strength-item"></li>
									</ul>
									
								</div>
								
							</div>

                			<div className="form-field">
								<label className='form-label' htmlFor="confirmPassword">{t('rePassLbl')}</label>
								<input type="password"
					 				 className={`form-input ${errors.confirmPassword ? 'input-danger' : ''}`}
									name="confirmPassword"
									id="confirmPassword"
                                    placeholder={t('rePassPH')}
									{...register( "confirmPassword", registerOptions.confirmPassword)}

								/>
									{touchedFields.confirmPassword && (
								<small className="text-danger">{errors?.confirmPassword?.message}</small>
									)}
								</div>

								<div className="form-field">
								<label className='form-label' htmlFor="langKey">{t('language')}</label>
								<select 
									className="form-input" 
									name="langkey"
									id="langKey"
									 {...register("langKey", {defaultValues: defaultLang})} 
									defaultValue={defaultLang}
									>
        							
        							<option value="en">English</option>
        							<option value="bg">Български</option>
      							</select>

								</div>
			
							<button type="submit" className="button form-btn" >{t('register')}</button>	
							
						</form>
					)}
						{showSuccess && (
							<div className="greenMsg">
								<p className="success">{t("successfullRegistration")}</p>
							</div>	)}		
        			<div className='yellowMsg'>

			<p>{t("regPageMsg1")} <Link to="/account/login" className="link">{t("signin")}</Link>, {t("regPageMsg2")}</p>
			
			<p>- {t("regPageMsgAdmin")}</p>
			<p>- {t("regPageMsgUser")}</p>
         
        </div>
		</div>
		
	
        </section>
    );
}
 
export default Register;