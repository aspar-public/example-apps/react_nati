import { useTranslation } from "react-i18next";
import { Link } from "react-router-dom";
import { useContext } from "react";
import AuthContext from "../../../util/AuthContext";


const Home = () => {
    const [t, i18n] = useTranslation('home');
    const authContext = useContext(AuthContext);
    const { currentUser } = authContext;

    return ( 
        <section className="homePage">
            <div className="hero-img" >
         
            </div>
            <div className="home-text">
                <h1 className="homeTitle">{t("welcomeMsg")} (ExampleApp01)</h1>
                <h2 className="homeSubtitle">{t("homePage")}</h2>
                <div className="homeMessages">
                    {currentUser ? 
                           <div className="greenMsg">
                           <p>{t("homePageMsg8")} "{currentUser?.login}"</p>
                       </div>
                    : <>
                    <div className="yellowMsg">
                    <p>{t("homePageMsg1")} <Link className="link" to="account/login">{t("homePageMsg2")}</Link> {t("homePageMsg3")}</p>
                    <p>{t("homePageMsg4")}</p>
                    <p>{t("homePageMsg5")}</p>
                </div>
                <div className="yellowMsg">
                    <p>{t("homePageMsg6")}? <Link className="link" to="account/register">{t("homePageMsg7")}</Link></p>
                </div>
                </>
                    }
                 
                 
                  
                </div>
                <p className="homeQuestions">{t("questions")} </p>
                <p className="homeQuestions">Trifon - Code Week 2024.01.05</p>
            </div>
        </section>
     );
}
 
export default Home;
