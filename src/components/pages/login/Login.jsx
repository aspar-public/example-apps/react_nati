import { useTranslation } from "react-i18next";
import i18next from "i18next";
import { Link, useNavigate } from "react-router-dom";
import { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import { loginUser } from "../../../services/UserServices";
import AuthContext from "../../../util/AuthContext";

 const Login = () => {
    const [t, i18n] = useTranslation('profile');
	const navigate = useNavigate();
	const { setJwtToken, setRole } = useContext(AuthContext);

	const { register, handleSubmit, name, getValues, setValue, formState, reset, isValid, setError,
		formState:{ errors, touchedFields, dirtyFields, isSubmitSuccessful} } = useForm({
				defaultValues: {
					login: "",
					password: "",
				},
				mode: "onTouched"
	});

	const onError = (errors) => console.log("Error", errors, "Touched:", touchedFields, "Dirty:", dirtyFields);
		const handleError = (errors) => {};

		const registerOptions = {
		  login: { required: t("errorUsernameRequired") },
		  password: {
			required: t("errorPasswordRequired")}
		}
	
	const onSubmit = async(data) => {
		try {
			const {token, role} = await loginUser(data);
			//console.log("Success", data);

			setJwtToken(token);
			setRole(role);
		  // console.log("User role from loginUser function", role, "Token from login:", token)

			navigate('/');
		} catch (error) {
			console.log(data);
			
			//handle error
			setError("login", {
				type: "manual",
				message: "Unable to login",
			  });
			console.error(error.message)
		}
	}


    return (
        <section className="registerPage">
            <div className="form-container">
				
					<h2 className="form-title">{t('login')}</h2>
						<form  className="form" onSubmit={handleSubmit(onSubmit, onError)}>
            				<div className="form-field">
								<label className='form-label' htmlFor="login">{t('usernameLbl')}</label>
								<input type="text"
									  className={`form-input ${errors.login ? 'input-danger' : ''}`}
									name="login"
									id="login"
                                    placeholder={t('usernamePH')}
									{...register("login", registerOptions.login)}
									
								/>
								{touchedFields.login && (
								<small className="text-danger">{errors?.login?.message}</small>
							)}
							</div>
                		

                			<div className="form-field">
								<label className='form-label' htmlFor="password">{t('passwordLbl')}</label>
								<input type="password" 
									className={`form-input ${errors.login ? 'input-danger' : ''}`}
									 name='password' 
									id='password'
                                    placeholder={t('passwordPH')}
                                    {...register("password", registerOptions.password)}
					 				/>
								{touchedFields.password && (
								<small className="text-danger">{errors?.password?.message}</small>
							)}
							</div>
               
			
							<button type="submit" className="button form-btn">{t('login')}</button>				
						</form>
        <div className='yellowMsg'>
            <p><Link className="link" to="/account/reset/request">{t("loginPageMsg1")}</Link></p>
			
         
        </div>
        <div className='yellowMsg'>
            <p>{t("loginPageMsg2")} <Link to="/account/register" className="link">{t("loginPageMsg3")}</Link></p>
        </div>
		</div>
		
	
        </section>
    );
}
export default Login;