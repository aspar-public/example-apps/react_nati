import { jwtDecode } from "jwt-decode";

const apiUrl = 'https://app-01.trifon.org/api/account/';


const editPassword = async (formData, token) => {
    try {
      
      const response = await fetch((apiUrl + "change-password"), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': '*/*',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify(formData),
      });
      console.log(response);
      if (!response.ok) {
        throw new Error(`Password edit failed: ${response.status} - ${response.statusText}`);
      } else {
        console.log('Password change successful.');
      };
     
      
    } catch (error) {
      console.error('Password change failed:', error.message);
      throw error;
    }
  };

  const resetPasswordInit = async (email) => {
    try {
      
      const response = await fetch((apiUrl + "reset-password/init"), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
        body: email,
      });
      console.log(response);
      if (!response.ok) {
        throw new Error(`Password reset init failed: ${response.status} - ${response.statusText}`);
      } else {
        console.log('Password reset init successful.');
      };
     
      
    } catch (error) {
      console.error('Password reset init failed:', error.message);
      throw error;
    }
  };

  
  const resetPasswordFinish = async (formData) => {
    try {
      
      const response = await fetch((apiUrl + "reset-password/finish"), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': '*/*',
        },
        body: JSON.stringify(formData),
      });
      console.log(response);
      if (!response.ok) {
        throw new Error(`Password reset finish failed: ${response.status} - ${response.statusText}`);
      } else {
        console.log('Password reset finish successful.');
      };
     
      
    } catch (error) {
      console.error('Password reset finish failed:', error.message);
      throw error;
    }
  };

    
  const activateAccount= async (key) => {
    try {
      
      const response = await fetch(("https://app-01.trifon.org/api/activate?key=" + key), {
        method: 'GET',
        headers: {
          'Content-Type': '*/*',
          'Accept': '*/*',
        },
      
      });
      console.log(response);
      if (!response.ok) {
        throw new Error(`Activation failed: ${response.status} - ${response.statusText}`);
      } else {
        console.log('Activation successful.');
      };
     
      
    } catch (error) {
      console.error('Activation failed:', error.message);
      throw error;
    }
  };

 export {
    editPassword,
    resetPasswordInit,
    resetPasswordFinish, 
    activateAccount
 }