import { jwtDecode } from "jwt-decode";

const apiUrl = 'https://app-01.trifon.org/api/';

const registerUser = async (formData) => {
    try {
      
      const response = await fetch((apiUrl + "register"), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
         
        },
        body: JSON.stringify(formData),
      });
      console.log(response);
      if (!response.ok) {
        throw new Error(`Registration failed: ${response.status} - ${response.statusText}`);
      }
       const data = await response.json(); 
      console.log('Registration successful:', data);
      return data;
      
    } catch (error) {
      console.error('Registration failed:', error.message);
      throw error;
    }
  };

  const loginUser = async (formData) => {
    const user = {
        username: formData.login,
        password: formData.password,
        rememberMe: true
    }
    //console.log(user);
    try {
      
      const response = await fetch((apiUrl + "authenticate"), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
      });
      //console.log(response);
      if (!response.ok) {
        throw new Error(`Login failed: ${response.status} - ${response.statusText}`);
      }
       const data = await response.json(); 
      console.log('Login successful:', data);

      const token = data.id_token;
      const userData = jwtDecode(token);
      const role = userData.auth;
      console.log(userData);
     
      localStorage.setItem('jwtToken', token);
      localStorage.setItem('userRole', role);
    
      return {token, role, data};
  
    } catch (error) {
      console.error('Login failed:', error.message);
      throw error;
    }
  };

  
  const getUserByToken = async (token) => {
    try {
      const response = await fetch((apiUrl + "account"), {
        method: 'GET',
        headers: {
          'Accept': '*/*',
          'Authorization': 'Bearer ' + token,
        },
        
      });
      
      if (!response.ok) {
        throw new Error(`Get user failed: ${response.status} - ${response.statusText}`);
      }
       const data = await response.json(); 

      //console.log("User:", data)
       return data;
      
    } catch (error) {
      console.error('Get user failed:', error.message);
      throw error;
    }
  }

  const editUser = async (formData, token) => {
    try {
      
      const response = await fetch((apiUrl + "account"), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': '*/*',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify(formData),
      });
      console.log(response);
      if (!response.ok) {
        throw new Error(`User edit failed: ${response.status} - ${response.statusText}`);
      } else {
        console.log('User edit successful:');
      };
     
      
    } catch (error) {
      console.error('User edit failed:', error.message);
      throw error;
    }
  };

  const editPassword = async (formData, token) => {
    try {
      
      const response = await fetch((apiUrl + "account/change-password"), {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': '*/*',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify(formData),
      });
      console.log(response);
      if (!response.ok) {
        throw new Error(`Password edit failed: ${response.status} - ${response.statusText}`);
      } else {
        console.log('Password change successful.');
      };
     
      
    } catch (error) {
      console.error('Password change failed:', error.message);
      throw error;
    }
  };



  
    export {
        registerUser,
        loginUser,
        getUserByToken,
        editUser,
     
    }