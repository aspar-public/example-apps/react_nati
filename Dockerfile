# pull official base image
FROM node:21-alpine3.17

# Install xdg-utils in order to solve: Error: spawn xdg-open ENOENT
RUN apk add --update xdg-utils

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
COPY package-lock.json ./
RUN npm install --silent
#RUN npm install react-scripts@5.0.1 -g --silent

# add app
COPY . ./
RUN npm run build

# start app
CMD ["npm", "run", "dev"]